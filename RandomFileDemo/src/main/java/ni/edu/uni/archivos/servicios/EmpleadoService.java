/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.archivos.servicios;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import ni.edu.uni.random.implement.EmpleadoDaoImplement;
import ni.edu.uni.random.main.Application;
import ni.edu.uni.random.model.EmpleadoModel;
import ni.edu.uni.random.pojo.Empleado;

/**
 *
 * @author yasser.membreno
 */
public class EmpleadoService implements IServices<Empleado>{
    private Scanner scan;
    private EmpleadoDaoImplement eDao;
    private EmpleadoModel eModel;

    public EmpleadoService(Scanner scan) throws IOException {
        this.scan = scan;
        eDao = new EmpleadoDaoImplement();                
    }
    
    public Empleado findById() throws IOException {

        System.out.println("Digite el id del activo fijo:");
        int id = scan.nextInt();

        Empleado empleado = eDao.findById(id);
        return  empleado;
    }
    
    @Override
    public void create() throws IOException {
        int id, cod;
        String cedula, nombres, apellidos, inss, fecha;
        double salario;
        
        System.out.println("id: ");
        id = scan.nextInt();
        
        System.out.println("Codigo empleado: ");
        cod = scan.nextInt();
        
        scan.nextLine();
        
        System.out.println("Cedula: ");
        cedula = scan.nextLine();
        
        System.out.println("Nombre: ");
        nombres = scan.nextLine();
        
        System.out.println("Apellidos: ");
        apellidos = scan.nextLine();
        
        System.out.println("INSS: ");
        inss = scan.nextLine();
        
        System.out.println("Fecha de contratacion [dd/mm/yyyy]");
        fecha = scan.next();
        
        System.out.println("Salario: ");
        salario = scan.nextDouble();
        
        Empleado e = new Empleado(id, cod, cedula, nombres, apellidos, inss,
                LocalDate.parse(fecha,DateTimeFormatter.ofPattern("dd/M/yyyy") ), salario);
        
        eDao.create(e);
        System.out.println("El empleado se guardo satisfactoriamente!");
        
    }

    @Override
    public int update() throws IOException {
        int id, cod;
        String cedula, nombres, apellidos, inss, fecha;
        double salario;

        System.out.println("id: ");
        id = scan.nextInt();

        Empleado emp = eDao.findById(id);

        if (emp == null){
            System.out.printf("El Identificador %d no es un identificador valido", id);
            return -1;
        }

        System.out.println("Codigo empleado: ");
        cod = scan.nextInt();

        scan.nextLine();

        System.out.println("Cedula: ");
        cedula = scan.nextLine();

        System.out.println("Nombre: ");
        nombres = scan.nextLine();

        System.out.println("Apellidos: ");
        apellidos = scan.nextLine();

        System.out.println("INSS: ");
        inss = scan.nextLine();

        System.out.println("Fecha de contratacion [dd/mm/yyyy]");
        fecha = scan.next();

        System.out.println("Salario: ");
        salario = scan.nextDouble();

        Empleado e = new Empleado(id, cod, cedula, nombres, apellidos, inss,
                LocalDate.parse(fecha,DateTimeFormatter.ofPattern("dd/M/yyyy") ), salario);

        int ideUPdated = eDao.update(e);
        if (ideUPdated > 0){
            System.out.println("\n\t-> El empleado se guardo satisfactoriamente!");
        }else{
            System.out.println("\n\t-> Algo ha ido mal");
        }
        return id;
    }

    @Override
    public boolean delete() throws IOException {
        System.out.println("id: ");
        int id = scan.nextInt();

        Empleado emp = eDao.findById(id);
        if (emp == null){
            System.out.printf("El Identificador %d no es un identificador valido", id);
            return false;
        }

        boolean flag = eDao.delete(emp);
        if (flag)
            System.out.println("\n\t-> Se ha eliminado el empleado correctamente");
        else
            System.out.println("\n\t-> Algo ha ido mal, no se elimino el empleado.");
        return flag;
    }

    @Override
    public void findAll() throws IOException {
        printHeader();
        for(Empleado e : eDao.findAll()){
            Application.print(e);
        }
        
    }
    
    private void printHeader(){
        System.out.format("%5s %5s %20s %20s %20s %10s %25s %5s\n", 
                "Id","Codigo","Cedula","Nombres","Apellidos","Inss","Fecha de contratacion","Salario");
    }
    
    public void findByFechaContratacion() throws IOException{
        String fecha;
        eModel = new EmpleadoModel();
        System.out.println("Fecha de contratacion [dd/mm/yyyy]");
        fecha = scan.next();
        
        eModel.setEmpleados(eDao.findByFechaContratacion(LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/M/yyyy"))));
        if(eModel.getEmpleados() == null){
            System.out.println("No se encontraron Empleados en ese rango de fecha!");
            return;
        }
        printHeader();
        for(Empleado e : eModel.getEmpleados()){
            Application.print(e);
        }
    }
    
    public  void printHeaders(){
        System.out.format("%5s %5s %20s %20s %20s %20s %10s \n","Id", "CodEmpleado","Nombres","Apellidos","INSS","Fecha_Contratacion","Salario");
    }
    
    public Empleado findByCod () throws IOException{
        
        System.out.println("Digite el codigo del empleado:");
        int cod = scan.nextInt();

        Empleado empleado = eDao.findByCod(cod);
        return empleado;
    }
    
    public Empleado findByCedula () throws IOException{
        System.out.println("Digite la cedula del empleado:");
        int cedula = scan.nextInt();

        Empleado empleado = eDao.findByCod(cedula);
        return empleado;
    }
    
    public Empleado findByInss () throws IOException {
        System.out.println("Digite el No. de INSS del empleado:");
        String inss = scan.nextLine();

        Empleado empleado = eDao.findByInss(inss);
        return empleado;
    }
    
    public void findBySalario () throws IOException {
        Double salario;
        eModel = new EmpleadoModel();
        System.out.println("Digite el salario de empleado:");
        salario = scan.nextDouble();
        
        eModel.setEmpleados(eDao.findBySalario(salario));
        if(eModel.getEmpleados() == null){
            System.out.println("No se encontraron Empleados!");
            return;
        }
        System.out.println("\n=============================================================================REGISTRO ENCONTRADO===========================================================================");
        printHeader();
        for(Empleado e : eModel.getEmpleados()){
            Application.print(e);  
        }
        System.out.println("=============================================================================================================================================================================\n");
    }
//    public void findByRangoFecha() throws IOException{
//        String fechaInicial, fechaFinal;        
//        eModel = new EmpleadoModel();
//        
//        System.out.println("Fecha inicial [dd/mm/yyyy]");
//        fechaInicial = scan.next();
//        
//        System.out.println("Fecha final [dd/mm/yyyy]");
//        fechaFinal = scan.next();
//        
//        eModel.setEmpleados(eDao.findByRangoFecha(
//                LocalDate.parse(fechaInicial, DateTimeFormatter.ofPattern("dd/M/yyyy")), 
//                LocalDate.parse(fechaFinal, DateTimeFormatter.ofPattern("dd/M/yyyy"))
//        ));
//        if(eModel.getEmpleados() == null){
//            System.out.println("No se encontraron Empleados en ese rango de fecha!");
//            return;
//        }
//        printHeader();
//        for(Empleado e : eModel.getEmpleados()){
//            ArchivosDatosApp.print(e);
//        }
//    }
    
    
   
}
