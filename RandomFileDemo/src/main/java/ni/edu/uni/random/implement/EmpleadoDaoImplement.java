/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.random.implement;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import ni.edu.uni.archivos.utils.SearchAlgorithms;
import ni.edu.uni.random.dao.IDaoEmpleado;
import ni.edu.uni.random.data.RandomConnection;
import ni.edu.uni.random.model.EmpleadoModel;
import ni.edu.uni.random.pojo.Empleado;

/**
 *
 * @author yasser.membreno
 */
public class EmpleadoDaoImplement implements IDaoEmpleado {

    private RandomConnection rcHeader;
    private RandomConnection rcData;
    private File fileHeader;
    private File fileData;
    private final String FILENAME_HEADER = "eHeader.dat";
    private final String FILENAME_DATA = "eData.dat";
    private final int SIZE = 166;

    public EmpleadoDaoImplement() throws IOException {
        init();
    }

    private void init() throws IOException {

        fileData = new File(FILENAME_DATA);
        fileHeader = new File(FILENAME_HEADER);

        rcData = new RandomConnection(fileData);
        rcHeader = new RandomConnection(fileHeader);

        if (rcHeader.getConnection().length() == 0) {
            rcHeader.getConnection().writeInt(0);//n
            rcHeader.getConnection().writeInt(0);//k
        }
    }

    @Override
    public Empleado findById(int id) throws IOException {
        Empleado e = null;

        if (id <= 0) {
            return e;
        }

        rcHeader.getConnection().seek(0);
        int n = rcHeader.getConnection().readInt();

        int index = SearchAlgorithms.randomBinarySearch(rcHeader.getConnection(), id, 0, n - 1);

        if (index <= 0) {
            return e;
        }

        long posData = (id - 1) * SIZE;

        rcData.getConnection().seek(posData);

        e = new Empleado();

        e.setId(rcData.getConnection().readInt());
        e.setCodEmpleado(rcData.getConnection().readInt());
        e.setCedula(rcData.getConnection().readUTF());
        e.setNombres(rcData.getConnection().readUTF());
        e.setApellidos(rcData.getConnection().readUTF());
        e.setInss(rcData.getConnection().readUTF());
        e.setFechaContratacion(LocalDate.ofEpochDay(rcData.getConnection().readLong()));
        e.setSalario(rcData.getConnection().readDouble());
        
        rcHeader.close();
        rcData.close();

        return e;
    }

    //TO-DO implementar metodo
    @Override
    public Empleado findByCod(int cod) throws IOException {
        Empleado e = null;

        if (cod <= 0) {
            return e;
        }

        rcHeader.getConnection().seek(0);
        int n = rcHeader.getConnection().readInt();

        for (int i=0; i < n ; i++){
            long posHeader = 8 + (i) * 4;

            rcHeader.getConnection().seek(posHeader);
            int key = rcHeader.getConnection().readInt();

            long posData = (key - 1) * SIZE;

            if(posData < 0) continue;

            rcData.getConnection().seek(posData);

            e = getCurrent();

            if (e.getCodEmpleado() == cod) break; else e = null;
        }

        rcHeader.close();
        rcData.close();

        return e;
    }
    //TO-DO implementar metodo
    @Override
    public Empleado findByCedula(String cedula) throws IOException {
        Empleado e = null;
        Optional<String> inssOptional = Optional.ofNullable(cedula);

        if (inssOptional.isEmpty() || cedula.length() == 0) {
            return e;
        }

        rcHeader.getConnection().seek(0);
        int n = rcHeader.getConnection().readInt();

        for (int i=0; i < n ; i++){
            long position = SetCurrentDataPosition(i);
            if (position < 0) continue;

            e = getCurrent();

            if (e.getCedula().equalsIgnoreCase(cedula)) break; else e = null;
        }

        rcHeader.close();
        rcData.close();

        return e;
    }
    //TO-DO implementar metodo
    @Override
    public Empleado findByInss(String inss) throws IOException {
        Empleado e = null;
        Optional<String> inssOptional = Optional.ofNullable(inss);

        if (inssOptional.isEmpty() || inss.length() == 0) {
            return e;
        }

        rcHeader.getConnection().seek(0);
        int n = rcHeader.getConnection().readInt();

        for (int i=0; i < n ; i++){
            long position = SetCurrentDataPosition(i);
            if (position < 0) continue;

            e = getCurrent();
            if (e.getInss().equalsIgnoreCase(inss)) break; else e = null;
        }

        rcHeader.close();
        rcData.close();

        return e;
    }
    //TO-DO implementar metodo
    @Override
    public Empleado[] findByFechaContratacion(LocalDate fecha) throws IOException {
        Empleado e = null;
        EmpleadoModel eModel = new EmpleadoModel();

        Optional<LocalDate> fechaOptional = Optional.ofNullable(fecha);

        if (fechaOptional.isEmpty() || fecha.isAfter(LocalDate.now())) {
            return eModel.getEmpleados();
        }

        rcHeader.getConnection().seek(0);
        int n = rcHeader.getConnection().readInt();

        for (int i=0; i < n ; i++){
            long position = SetCurrentDataPosition(i);
            if (position < 0) continue;

            e = getCurrent();
            if (e.getFechaContratacion().equals(fecha)) eModel.add(e);
        }

        rcHeader.close();
        rcData.close();

        return eModel.getEmpleados();
    }
    //TO-DO implementar metodo
    @Override
    public Empleado[] findBySalario(double salario) throws IOException {
        Empleado e = null;
        EmpleadoModel eModel = new EmpleadoModel();

        rcHeader.getConnection().seek(0);
        int n = rcHeader.getConnection().readInt();

        for (int i=0; i < n ; i++){
            long position = SetCurrentDataPosition(i);
            if (position < 0) continue;

            e = getCurrent();
            if (e.getSalario() == salario) eModel.add(e);
        }

        rcHeader.close();
        rcData.close();

        return eModel.getEmpleados();
    }

    @Override
    public void create(Empleado t) throws IOException {
        if (t == null) {
            return;
        }

        rcHeader.getConnection().seek(0);

        int n = rcHeader.getConnection().readInt();
        int k = rcHeader.getConnection().readInt();

        long posData = k * SIZE;

        rcData.getConnection().seek(posData);
        ++k;
        writeObject(t, k);
        rcHeader.getConnection().seek(0);
        rcHeader.getConnection().writeInt(++n);
        rcHeader.getConnection().writeInt(k);

        long posHeader = 8 + (k - 1) * 4;

        rcHeader.getConnection().seek(posHeader);
        rcHeader.getConnection().writeInt(k);

        rcHeader.close();
        rcData.close();
    }
    //TO-DO implementar metodo
    @Override
    public int update(Empleado t) throws IOException {
        if (t == null || t.getId() <= 0) {
            return 0;
        }

        rcHeader.getConnection().seek(0);

        int n = rcHeader.getConnection().readInt();
        int k = rcHeader.getConnection().readInt();

        int index = SearchAlgorithms.randomBinarySearch(rcHeader.getConnection(), t.getId(), 0, n - 1);

        if (index <= 0) {
            return -1;
        }

        long posData = (t.getId() -1) * SIZE;

        rcData.getConnection().seek(posData);

        writeObject(t, t.getId());

        rcHeader.close();
        rcData.close();

        return t.getId();
    }
    //TO-DO implementar metodo
    @Override
    public boolean delete(Empleado t) throws IOException {
        if (t == null || t.getId() <= 0) {
            return false;
        }

        rcHeader.getConnection().seek(0);

        int n = rcHeader.getConnection().readInt();
        int k = rcHeader.getConnection().readInt();

        int index = SearchAlgorithms.randomBinarySearch(rcHeader.getConnection(), t.getId(), 0, n - 1);

        rcHeader.close();
        rcData.close();

        if (index <= 0) {
            return false;
        }

        Empleado[] emps = findAll();
        fileData.delete();
        fileHeader.delete();

        init();

        boolean flag = false;
        for (Empleado e:
             emps) {
            if (e.getId() != t.getId()) create(e) ; else flag= !flag;
        }
//
//        long posHeader = index;
//        rcHeader.getConnection().seek(posHeader);
//        rcHeader.getConnection().writeInt(-1);
//
//        rcHeader.close();
//        rcData.close();

        return findById(t.getId()) == null;
    }

    @Override
    public Empleado[] findAll() throws IOException {
        Empleado e = null;
        EmpleadoModel eModel = new EmpleadoModel();

        rcHeader.getConnection().seek(0);
        int n = rcHeader.getConnection().readInt();

        for (int i = 0; i < n; i++) {
            long posHeader = 8 + (i) * 4;

            rcHeader.getConnection().seek(posHeader);
            int key = rcHeader.getConnection().readInt();

            long posData = (key - 1) * SIZE;
            rcData.getConnection().seek(posData);

            e = new Empleado();

            e.setId(rcData.getConnection().readInt());
            e.setCodEmpleado(rcData.getConnection().readInt());
            e.setCedula(rcData.getConnection().readUTF());
            e.setNombres(rcData.getConnection().readUTF());
            e.setApellidos(rcData.getConnection().readUTF());
            e.setInss(rcData.getConnection().readUTF());
            e.setFechaContratacion(LocalDate.ofEpochDay(rcData.getConnection().readLong()));
            e.setSalario(rcData.getConnection().readDouble());

            eModel.add(e);
        }
        rcHeader.close();
        rcData.close();

        return eModel.getEmpleados();
    }


    /**
     * Metodos de ayuda
     * */

    private Empleado getCurrent() throws IOException {
        Empleado e = new Empleado();

        e.setId(rcData.getConnection().readInt());
        e.setCodEmpleado(rcData.getConnection().readInt());
        e.setCedula(rcData.getConnection().readUTF());
        e.setNombres(rcData.getConnection().readUTF());
        e.setApellidos(rcData.getConnection().readUTF());
        e.setInss(rcData.getConnection().readUTF());
        e.setFechaContratacion(LocalDate.ofEpochDay(rcData.getConnection().readLong()));
        e.setSalario(rcData.getConnection().readDouble());
        return e;
    }

    private void writeObject(Empleado t, int id) throws IOException {
        rcData.getConnection().writeInt(id);
        rcData.getConnection().writeInt(t.getCodEmpleado());
        rcData.getConnection().writeUTF(t.getCedula());
        rcData.getConnection().writeUTF(t.getNombres());
        rcData.getConnection().writeUTF(t.getApellidos());
        rcData.getConnection().writeUTF(t.getInss());
        rcData.getConnection().writeLong(t.getFechaContratacion().toEpochDay());
        rcData.getConnection().writeDouble(t.getSalario());
    }

    private long SetCurrentDataPosition(int i) throws IOException {
        long posHeader = 8 + (i) * 4;

        rcHeader.getConnection().seek(posHeader);
        int key = rcHeader.getConnection().readInt();

        long posData = (key - 1) * SIZE;

        if(posData < 0) return -1;

        rcData.getConnection().seek(posData);
        return posData;
    }

}
