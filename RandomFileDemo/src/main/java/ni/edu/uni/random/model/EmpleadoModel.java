/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.random.model;

import java.util.Arrays;
import java.util.List;
import ni.edu.uni.random.pojo.Empleado;

/**
 *
 * @author yasser.membreno
 */
public class EmpleadoModel {
    private List<Empleado> empleados;

    public EmpleadoModel() {
    }
    
    public void add(Empleado value){
        if(empleados == null){
            empleados = new List<Empleado>;
            empleados.set(0, value);
            return;
        }

        empleados = Arrays.copyOf(empleados, empleados.size+1);
        empleados<empleados.length - 1> = value;        
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }
    
     
    
}
